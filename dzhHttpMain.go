package main

import (
  "net/http"
  "fmt"
  "time"
 // "strconv"
)

func homeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "helllo")
}
func main() {

	mux := http.NewServeMux()
	//files := http.FileServer(http.Dir("F:\\temp\\temp\\public"))
	files := http.FileServer(http.Dir("h:\\www"))
	mux.Handle("/static/", http.StripPrefix("/static/", files))

	mux.HandleFunc("/home", index)
	mux.HandleFunc("/", index)
	mux.HandleFunc("/bye", sayBye)
	mux.HandleFunc("/gettime", getTime)

	server := &http.Server{
		Addr:"0.0.0.0:80",
		Handler:mux,
	}
	fmt.Println("http service is start")
	err := server.ListenAndServe()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("end")
}

func index(w http.ResponseWriter, r *http.Request) {
	//fmt.Println(time.Now().Format("20060102150405"), "Redirect to index.html")
	fmt.Println(time.Now().Format("2006-01-02 15:04:05"), "Redirect to index.html")
	http.Redirect(w, r, "/static", http.StatusTemporaryRedirect)
	//http.Redirect(w, r, "/bye", http.StatusTemporaryRedirect)
	return
}

func sayBye(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("bye bye ,shutdown the server"))     // 没有输出
	//err := server.Shutdown(nil)
	//if err != nil {
		fmt.Println([]byte("shutdown the server err"))
	//}
}

func getTime(w http.ResponseWriter, r *http.Request) {
	//timeUnix:=time.Now().Unix()            //单位s,打印结果:1491888244
	//timeUnixNano:=time.Now().UnixNano()    //单位纳秒,打印结果：1491888244752784461
	//fmt.Printf("时间戳（秒）：%v;\n", time.Now().Unix())
	//fmt.Printf("时间戳（纳秒）：%v;\n",time.Now().UnixNano())
	//fmt.Printf("时间戳（毫秒）：%v;\n",time.Now().UnixNano() / 1e6)
	//fmt.Printf("时间戳（纳秒转换为秒）：%v;\n",time.Now().UnixNano() / 1e9)
	timeUnix:=time.Now().UnixNano() / 1e6
	fmt.Println(timeUnix)
	//fmt.Println(w, timeUnix)
	str:=fmt.Sprintf("{\"result\":true,\"time\":%d}", timeUnix)
	w.Write([]byte(str))
}



